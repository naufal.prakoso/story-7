import time
import unittest
import os
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from appstory7gan.views import index
from story7bro.settings import BASE_DIR

# Create your tests here.
class UnitTest(TestCase):
    def test_app_url_is_exist(self):
        self.client = Client()
        response = self.client.get('')
        self.assertEqual(response.status_code,200)

    def test_app_template_is_correct (self):
        self.client = Client()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'profile.html')
